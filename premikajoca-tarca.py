
from tkinter import *
import math

class Tarca():
    def __init__(self, master):
        self.width = 400
        self.height = 400
        self.canvas = Canvas(master, width=self.width, height=self.height)
        self.canvas.grid(row = 1, column = 0, columnspan = 3)
        self.canvas.bind("<Button-1>", self.zapisi_tocke)

        self.x = 50
        self.y = 100
        self.vx = 2
        self.vy = 3
        self.i = 0

        self.tocke = DoubleVar(master, value=0)
        self.sest = DoubleVar(master, value=0)
        self.povpr = DoubleVar(master, value=0)
        label_tocke = Label(master, textvariable = self.tocke)
        label_sest = Label(master, textvariable = self.sest)
        label_povpr = Label(master, textvariable = self.povpr)
        label_tocke.grid(row = 0, column = 0, sticky="ew")
        label_sest.grid(row = 0, column = 1, sticky="ew")
        label_povpr.grid(row = 0, column = 2, sticky="ew")
        Label(master,text='Točke:').grid(row = 0, column = 0, sticky="w")
        Label(master,text='Seštevek:').grid(row = 0, column = 1,sticky="w")
        Label(master,text='Povprečje:').grid(row = 0, column = 2,sticky="w")

        menu = Menu(master)
        master.config(menu=menu)

        datoteka_menu = Menu(menu)
        menu.add_cascade(label="Datoteka", menu=datoteka_menu)
        datoteka_menu.add_command(label="Novo",command=self.novo)
        datoteka_menu.add_command(label="Shrani",command=self.shrani)
        datoteka_menu.add_command(label="Odpri",command=self.odpri)
        datoteka_menu.add_separator()
        datoteka_menu.add_command(label="Izhod", command=master.destroy)


        self.krogi = []
        self.krogi.append((self.canvas.create_oval(self.x-50, self.y-50, self.x+50, self.y+50, fill = "red"), 50))
        self.krogi.append((self.canvas.create_oval(self.x-35, self.y-35, self.x+35, self.y+35, fill = "yellow"), 35))
        self.krogi.append((self.canvas.create_oval(self.x-15, self.y-15, self.x+15, self.y+15, fill = "green"), 15))

        self.animacija()

    toocke = 0
    sestevekk = 0
    povprecjee = 0
    
    def novo(self):
        self.tocke.set(0)
        self.sest.set(0)
        self.povpr.set(0)

    def shrani(self):
        ime = filedialog.asksaveasfilename()
        with open(ime,"w") as f:
            f.write(str(self.sestevekk)+",")
            f.write(str(self.povprecjee))
        return f

    def odpri (self):
        ime = filedialog.askopenfilename()
        if ime == "":
            return
        with open(ime,"r")as g:
            sez = []
            o = g.read()
            sez = o.split(",")
             
            self.sestevekk = int(sez[0])
            self.povprecjee = float(sez[1])
            self.sest.set(int(sez[0]))
            self.povpr.set(float(sez[1]))            

    def animacija(self):
        self.premik()
        self.canvas.after(10, self.animacija)

    def premik(self):
        self.x = self.x + self.vx  
        self.y = self.y + self.vy
        if self.x - 50 < 0:
            self.vx = -self.vx
        if self.x + 50 > self.width:
            self.vx = -self.vx
        if self.y - 50 < 0:
            self.vy = -self.vy
        if self.y + 50 > self.height:
            self.vy = -self.vy
            
        for e, r in self.krogi:
            self.canvas.coords(e, self.x - r, self.y - r, self.x + r, self.y + r)

    def zapisi_tocke(self, event):
        x = event.x
        y = event.y
        tocke1 = self.sest.get()
        tocke2 = 0
        if (x - self.x)**2 + (y - self.y)**2 < 15**2 :
            tocke2 = 15
        elif (x - self.x)**2 + (y - self.y)**2 < 35**2 :
            tocke2 = 10
        elif (x - self.x)**2 + (y - self.y)**2 < 50**2 :
            tocke2 = 5

        self.i += 1
        tocke = math.floor(tocke1 + tocke2)
        povpr = round(tocke / self.i, 1)

        self.tocke.set(tocke2)
        self.sest.set(tocke)
        self.povpr.set(povpr)
        
        self.toocke = tocke2
        self.povprecjee = povpr
        self.sestevekk = tocke
        




root = Tk()
Tarca(root)
root.mainloop()
